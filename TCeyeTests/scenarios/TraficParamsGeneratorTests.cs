﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TCeye.conflict;
using TCeye.scenarios;
using TCeye.tools;

namespace TCeyeTests.scenarios
{
    [TestClass]
    public class TraficParamsGeneratorTests
    {
        [TestMethod]
        public void createRandomTraficParamsTest()
        {
            TraficParams trafic = TraficParamsGenerator.createRandomTraficParams();

            Assert.IsTrue(Math.Abs(trafic.bearing) <= 100 * Consts.DEG_2_RAD);
        }
    }
}
