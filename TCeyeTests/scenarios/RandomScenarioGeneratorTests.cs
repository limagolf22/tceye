﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using TCeye.scenarios;
using TCeye.simconnect;

namespace TCeyeTests.scenarios
{
    [TestClass]
    public class RandomScenarioGeneratorTests
    {
        [TestMethod]
        public void createRandomScenarioGeneratorTest()
        {
            SimProxy sp = Mock.Of<SimProxy>();

            Mock.Get(sp).Setup(m => m.getLastPosOfPlayer()).Returns(new TCeye.POS_struct());
            Mock.Get(sp).Setup(m => m.callAIAircraftCreation(It.IsAny<TCeye.POS_struct>()));

            RandomScenarioProcessor scenarioGenerator = new RandomScenarioProcessor(sp);

            scenarioGenerator.start();
            scenarioGenerator.stop();
            Assert.IsTrue(true);
        }
    }
}
