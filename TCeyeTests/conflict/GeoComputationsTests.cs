﻿using GeoComputations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TCeyeTests.conflict
{
    [TestClass]
    public class GeoComputationsTests
    {
        [TestMethod]
        public void displacePositionFromTest()
        {
            GeoPoint expectedPoint1 = new GeoPoint(0.0, 0.804544410433286, 2000);
            GeoPoint expectedPoint2 = new GeoPoint(0.0104871506807743, 0.804544410433286, 1000);
            GeoPoint expectedPoint3 = new GeoPoint(-0.0104871506807743, 0.804544410433286, 1000);
            GeoPoint expectedPoint4 = new GeoPoint(-0.0104871506807743, 0.79, 1000);

            GeoPoint point = new GeoPoint(0.0, 0.79, 1000);

            point.displacePositionFrom(0.0, 50.0, 1000);

            Assert.AreEqual((float)expectedPoint1.lat, (float)point.lat);
            Assert.AreEqual(expectedPoint1.lon, point.lon);
            Assert.AreEqual(expectedPoint1.altitude, point.altitude);

            point.displacePositionFrom(Math.PI / 2, 25.0, -1000);

            Assert.AreEqual((float)expectedPoint2.lat, (float)point.lat);
            Assert.AreEqual((float)expectedPoint2.lon, (float)point.lon);
            Assert.AreEqual(expectedPoint2.altitude, point.altitude);

            point.displacePositionFrom(3 * Math.PI / 2, 50.0, 0);

            Assert.AreEqual((float)expectedPoint3.lat, (float)point.lat);
            Assert.AreEqual((float)expectedPoint3.lon, (float)point.lon);
            Assert.AreEqual(expectedPoint3.altitude, point.altitude);

            point.displacePositionFrom(Math.PI, 50.0, 0);

            Assert.AreEqual((float)expectedPoint4.lat, (float)point.lat);
            Assert.AreEqual((float)expectedPoint4.lon, (float)point.lon);
            Assert.AreEqual(expectedPoint4.altitude, point.altitude);
        }
    }
}
