﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TCeye;
using TCeye.conflict;
using TCeye.tools;

namespace TCeyeTests.conflict
{
    [TestClass]
    public class ConflictGeneratorTests
    {
        [TestMethod]
        public void createTraficTest()
        {
            POS_struct target_struct = new POS_struct
            {
                altitude = 3000.0f,
                heading = 000,
                kohlsman = 29.92f,
                latitude = 45.0 * Consts.DEG_2_RAD,
                longitude = 1.0 * Consts.DEG_2_RAD,
                pitch = 0.0f,
                roll = 0.0f,
                speed = 100f
            };

            POS_struct pos_struct1 = ConflictGenerator.createTrafic(target_struct, 0, 60, 200, 180 * Consts.DEG_2_RAD);

            POS_struct expected_struct = new POS_struct
            {
                altitude = 3200.0f,
                heading = (float)(180 * Consts.DEG_2_RAD),
                kohlsman = 29.92f,
                latitude = 46.0 * Consts.DEG_2_RAD,
                longitude = 1.0 * Consts.DEG_2_RAD,
                pitch = 0.0f,
                roll = 0.0f,
                speed = -1f
            };

            comparePosStructs(expected_struct, pos_struct1);



            POS_struct pos_struct2 = ConflictGenerator.createTrafic(target_struct, 90 * Consts.DEG_2_RAD, 60 * Math.Sqrt(2) / 2, 200, 270 * Consts.DEG_2_RAD);

            expected_struct = new POS_struct
            {
                altitude = 3200.0f,
                heading = (float)(270 * Consts.DEG_2_RAD),
                kohlsman = 29.92f,
                latitude = 45 * Consts.DEG_2_RAD,
                longitude = 2 * Consts.DEG_2_RAD,
                pitch = 0.0f,
                roll = 0.0f,
                speed = -1f
            };

            comparePosStructs(expected_struct, pos_struct2);

        }

        [TestMethod]
        public void createCrossingTraficTest()
        {
            POS_struct target_struct = new POS_struct
            {
                altitude = 3000.0f,
                heading = 000,
                kohlsman = 29.92f,
                latitude = (45.0 - 5.0 / 60) * Consts.DEG_2_RAD,
                longitude = 1.0 * Consts.DEG_2_RAD,
                pitch = 0.0f,
                roll = 0.0f,
                speed = 60f
            };

            TraficPlane trafic = new TraficPlane(60, 48);

            POS_struct pos_struct1 = ConflictGenerator.createCrossingTrafic(target_struct, 300, trafic, Math.PI / 2);

            POS_struct expected_struct = new POS_struct
            {
                altitude = 3000.0f,
                heading = (float)(270 * Consts.DEG_2_RAD),
                kohlsman = 29.92f,
                latitude = (45.0 + 0.0 / 60) * Consts.DEG_2_RAD,
                longitude = (1.0 + 5.0 / 60 * 2 / Math.Sqrt(2)) * Consts.DEG_2_RAD,
                pitch = 0.0f,
                roll = 0.0f,
                speed = 60
            };

            comparePosStructs(expected_struct, pos_struct1);

            pos_struct1 = ConflictGenerator.createCrossingTrafic(target_struct, 150, trafic, 0);

            POS_struct expected_struct2 = new POS_struct
            {
                altitude = 3000.0f,
                heading = (float)(180 * Consts.DEG_2_RAD),
                kohlsman = 29.92f,
                latitude = 45.0 * Consts.DEG_2_RAD,
                longitude = 1.0 * Consts.DEG_2_RAD,
                pitch = 0.0f,
                roll = 0.0f,
                speed = 60
            };

            comparePosStructs(expected_struct2, pos_struct1);
        }

        void comparePosStructs(POS_struct expected_struct, POS_struct pos_struct)
        {
            Assert.AreEqual(expected_struct.altitude, pos_struct.altitude);
            Assert.AreEqual(expected_struct.heading, pos_struct.heading);
            Assert.AreEqual(expected_struct.latitude, pos_struct.latitude);
            Assert.AreEqual((float)expected_struct.longitude, (float)pos_struct.longitude);
            Assert.AreEqual(expected_struct.speed, pos_struct.speed);
        }
    }
}
