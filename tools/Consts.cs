﻿using System;

namespace TCeye.tools
{
    public static class Consts
    {
        public static double DEG_2_RAD = Math.PI / 180;

        public static double NM_TO_FT = 1852 / 0.3048;
    }
}
