﻿namespace TCeye.conflict
{
    public class TraficPlane
    {
        public int cruiseSpeed { get; }
        public int glideRatio { get; }

        public TraficPlane(int _cruiseSpeed, int _glideRatio)
        {
            cruiseSpeed = _cruiseSpeed;
            glideRatio = _glideRatio;
        }
    }
}
