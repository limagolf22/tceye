﻿using System;
using TCeye.tools;

namespace GeoComputations
{
    public class GeoPoint
    {
        public double lon, lat, altitude;

        public GeoPoint(double _lon, double _lat, double _altitude)
        {
            lon = _lon;
            lat = _lat;
            altitude = _altitude;
        }

        GeoPoint(GeoPoint point)
        {
            lon = point.lon;
            lat = point.lat;
            altitude = point.altitude;
        }

        public void displacePositionFrom(double bearing, double distance, double separation)
        {
            addSteps(distance * Math.Sin(bearing), distance * Math.Cos(bearing), separation);
        }

        public void addSteps(double deltaX, double deltaY, double deltaZ)
        {
            lon += deltaX / Math.Cos(lat) / 60 * Consts.DEG_2_RAD;
            lat += deltaY / 60 * Consts.DEG_2_RAD;
            altitude += deltaZ;
        }
    }
    class Geometry
    {
        public static double sumAngles(double angle1, double angle2)
        {
            double angleRes = angle1 + angle2;
            if (angleRes > Math.PI * 2)
            {
                angleRes -= (double)(Math.PI * 2);
            }
            else if (angleRes < 0)
            {
                angleRes += (double)(Math.PI * 2);
            }
            return angleRes;
        }
    }
}
