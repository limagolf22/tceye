﻿namespace TCeye.conflict
{
    public struct TraficParams
    {
        public int timeSep;
        public float bearing;
        public double verticalSep;
        public TraficPlane trafic;
    }
}
