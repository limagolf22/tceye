﻿using GeoComputations;
using System;
using TCeye.tools;

namespace TCeye.conflict
{
    public class ConflictGenerator
    {
        public static POS_struct createCrossingTrafic(POS_struct targetPosition, int gap, TraficPlane trafic, double bearing)
        {
            GeoPoint crossingPoint = getPointFromStruct(targetPosition);
            crossingPoint.displacePositionFrom(targetPosition.heading, gap * targetPosition.speed / 3600, 0);

            crossingPoint.displacePositionFrom(Geometry.sumAngles(targetPosition.heading, bearing), (float)(gap * trafic.cruiseSpeed) / 3600, 0);

            POS_struct posRes = getStructFromPoint(crossingPoint);

            posRes.speed = trafic.cruiseSpeed;
            posRes.heading = (float)Geometry.sumAngles(targetPosition.heading, Geometry.sumAngles(bearing, Math.PI));
            float deltaZ = (float)(4 * gap * targetPosition.speed / 3600 * Consts.NM_TO_FT / (float)trafic.glideRatio);
            posRes.altitude += deltaZ;
            System.Diagnostics.Debug.WriteLine("deltaZ : " + deltaZ);
            return posRes;
        }

        public static POS_struct createTrafic(POS_struct targetPosition, double bearing, double distance, double separation, double heading)
        {
            GeoPoint traficPos = getPointFromStruct(targetPosition);
            traficPos.displacePositionFrom(bearing, distance, separation);
            POS_struct traficPosStruct = getStructFromPoint(traficPos);
            traficPosStruct.heading = (float)heading;

            return traficPosStruct;
        }

        private static GeoPoint getPointFromStruct(POS_struct posStruct)
        {
            return new GeoPoint(posStruct.longitude, posStruct.latitude, posStruct.altitude);
        }

        private static POS_struct getStructFromPoint(GeoPoint point)
        {
            POS_struct pos_struct = new POS_struct();
            pos_struct.kohlsman = 29.92f;
            pos_struct.altitude = (float)point.altitude;
            pos_struct.latitude = point.lat;
            pos_struct.longitude = point.lon;
            pos_struct.roll = 0f;
            pos_struct.pitch = 0f;
            pos_struct.heading = 0f;
            pos_struct.speed = -1.0f;

            return pos_struct;
        }

    }
}
