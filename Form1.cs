﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TCeye.conflict;
using TCeye.events;
using TCeye.scenarios;
using TCeye.simconnect;

namespace TCeye
{
    public partial class Form1 : Form
    {

        private SimconnectProxy simconnectProxy;

        private ScenarioProcessor scenarioProcessor;

        private bool isProcessorActive = false;

        private System.Timers.Timer timer;

        private List<TraficPlane> trafics = new List<TraficPlane>();

        private static int freqWS = 2;
        private static string POS_STR = "";

        public Form1()
        {
            InitializeComponent();
            simconnectProxy = new SimconnectProxy();

            scenarioProcessor = new RandomScenarioProcessor(simconnectProxy);

            trafics.Add(new TraficPlane(150, 48));

            simconnectProxy.ConnectionStatusChanged += SimconnectProxy_ConnectionStatusChanged;
            simconnectProxy.PositionChanged += OnPositionChanged;

            simconnectProxy.initSimEvents(this.Handle, freqWS);
        }

        protected override void DefWndProc(ref Message m)
        {
            if (m.Msg == 0x402)
            {
                if (simconnectProxy.simconnect != null)
                {
                    simconnectProxy.simconnect.ReceiveMessage();
                }
            }
            else
            {
                base.DefWndProc(ref m);
            }
        }

        private void OnPositionChanged(Object source, POS_struct pos)
        {
            //POS_struct pos = simconnectProxy.getLastPos();

            //datumTextBox.Text = "okok";
            datumRichTextBox.Text = pos.latitude + "\n" + pos.longitude + "\n" + pos.speed + "\n" + pos.heading;
        }

        private void SimconnectProxy_ConnectionStatusChanged(object sender, Status e)
        {
            StatusLabel.Text = "Status : " + e;
        }

        private void launchFlightButton_Click(object sender, EventArgs e)
        {
            POS_struct pos = ConflictGenerator.createCrossingTrafic(simconnectProxy.getLastPosOfPlayer(), 5, trafics.FirstOrDefault(), Math.PI / 2);
            simconnectProxy.callAIAircraftCreation(pos);
        }

        private void launchProcessorButton_Click(object sender, EventArgs e)
        {
            if (isProcessorActive)
            {
                scenarioProcessor.stop();
                isProcessorActive = false;
            }
            else
            {
                scenarioProcessor.start();
                isProcessorActive = true;
            }
        }
    }
}
