﻿using Microsoft.Extensions.Logging;
using System.Timers;
using TCeye.conflict;
using TCeye.simconnect;

namespace TCeye.scenarios
{
    public class RandomScenarioProcessor : ScenarioProcessor
    {
        private readonly ILogger logger;

        SimProxy simProxy;

        Timer timer;

        public RandomScenarioProcessor(SimProxy simConnectProxyArg)
        {
            logger = LoggerFactory.Create(builder => builder.AddDebug()).CreateLogger(GetType().ToString());

            simProxy = simConnectProxyArg;

            timer = new Timer(20000);
            // Hook up the Elapsed event for the timer. 
            timer.Elapsed += OnRandomScenarioTrigerred;
            timer.AutoReset = true;
            timer.Enabled = true;
            logger.LogInformation("Random Scenario Generator initialized");
        }

        private void OnRandomScenarioTrigerred(object sender, ElapsedEventArgs e)
        {
            generateRandomFlight();
        }

        private void generateRandomFlight()
        {
            TraficParams traficParams = TraficParamsGenerator.createRandomTraficParams();

            POS_struct pos = ConflictGenerator.createCrossingTrafic(simProxy.getLastPosOfPlayer(), traficParams.timeSep, traficParams.trafic, traficParams.bearing);
            simProxy.callAIAircraftCreation(pos);
            logger.LogInformation("Flight Randomly generated");
        }

        public void start()
        {
            timer.Start();
            logger.LogInformation("Random Scenario Generator started");
        }

        public void stop()
        {
            timer.Stop();
            logger.LogInformation("Random Scenario Generator stopped");
        }
    }
}
