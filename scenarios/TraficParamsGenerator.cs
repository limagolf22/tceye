﻿using Microsoft.Extensions.Logging;
using System;
using TCeye.conflict;
using TCeye.tools;

namespace TCeye.scenarios
{
    public class TraficParamsGenerator
    {
        static readonly ILogger logger = LoggerFactory.Create(builder => builder.AddDebug()).CreateLogger(nameof(TraficParamsGenerator));

        public static TraficParams createRandomTraficParams()
        {
            Random rdm = new Random();

            float rdmBearing = (float)(rdm.Next(-100, 100) * Consts.DEG_2_RAD);
            int rdmSpeed = rdm.Next(60, 70);
            int rdmImpactTime = rdm.Next(10, 35);

            TraficPlane plane = new TraficPlane(rdmSpeed, 48);

            TraficParams traffic = new TraficParams
            {
                timeSep = rdmImpactTime,
                bearing = rdmBearing,
                verticalSep = 0.0,
                trafic = plane
            };
            logger.LogInformation("Trafic created coming from the {bearing}°, at {speed}kts, above {height} in {time}s", traffic.bearing / Consts.DEG_2_RAD, traffic.trafic.cruiseSpeed, traffic.verticalSep, traffic.timeSep);
            return traffic;
        }
    }
}
