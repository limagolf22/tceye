﻿namespace TCeye.scenarios
{
    public interface ScenarioProcessor
    {
        void start();

        void stop();
    }
}
