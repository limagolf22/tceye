﻿
namespace TCeye
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.launchFlightButton = new System.Windows.Forms.Button();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.datumTextBox = new System.Windows.Forms.TextBox();
            this.datumRichTextBox = new System.Windows.Forms.RichTextBox();
            this.launchProcessorButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // launchFlightButton
            // 
            this.launchFlightButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.launchFlightButton.Location = new System.Drawing.Point(149, 359);
            this.launchFlightButton.Name = "launchFlightButton";
            this.launchFlightButton.Size = new System.Drawing.Size(144, 23);
            this.launchFlightButton.TabIndex = 0;
            this.launchFlightButton.Text = "launch flight";
            this.launchFlightButton.UseVisualStyleBackColor = true;
            this.launchFlightButton.Click += new System.EventHandler(this.launchFlightButton_Click);
            // 
            // StatusLabel
            // 
            this.StatusLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(339, 9);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(43, 13);
            this.StatusLabel.TabIndex = 1;
            this.StatusLabel.Text = "Status :";
            // 
            // datumTextBox
            // 
            this.datumTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.datumTextBox.Location = new System.Drawing.Point(248, 26);
            this.datumTextBox.Name = "datumTextBox";
            this.datumTextBox.ReadOnly = true;
            this.datumTextBox.ShortcutsEnabled = false;
            this.datumTextBox.Size = new System.Drawing.Size(209, 20);
            this.datumTextBox.TabIndex = 2;
            // 
            // datumRichTextBox
            // 
            this.datumRichTextBox.Enabled = false;
            this.datumRichTextBox.Location = new System.Drawing.Point(248, 53);
            this.datumRichTextBox.Name = "datumRichTextBox";
            this.datumRichTextBox.Size = new System.Drawing.Size(209, 96);
            this.datumRichTextBox.TabIndex = 3;
            this.datumRichTextBox.Text = "";
            // 
            // launchProcessorButton
            // 
            this.launchProcessorButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.launchProcessorButton.Location = new System.Drawing.Point(149, 388);
            this.launchProcessorButton.Name = "launchProcessorButton";
            this.launchProcessorButton.Size = new System.Drawing.Size(144, 23);
            this.launchProcessorButton.TabIndex = 4;
            this.launchProcessorButton.Text = "launch processor";
            this.launchProcessorButton.UseVisualStyleBackColor = true;
            this.launchProcessorButton.Click += new System.EventHandler(this.launchProcessorButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 426);
            this.Controls.Add(this.launchProcessorButton);
            this.Controls.Add(this.datumRichTextBox);
            this.Controls.Add(this.datumTextBox);
            this.Controls.Add(this.StatusLabel);
            this.Controls.Add(this.launchFlightButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button launchFlightButton;
        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.TextBox datumTextBox;
        private System.Windows.Forms.RichTextBox datumRichTextBox;
        private System.Windows.Forms.Button launchProcessorButton;
    }
}

