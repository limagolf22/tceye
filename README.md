# TCeye

TCeye is a tool developed with **.NET Framework** that uses the **Simconnect API** to generates random conflicting trafics in **MSFS**.



## Getting started

The project can be opened in **Visual Studio** with .NET Framework.

Once the UI is opened, click the *launch processor* button to start the random conflicts generation.


## Usefull references

[SimConnect](https://docs.flightsimulator.com/html/Programming_Tools/SimConnect/SimConnect_SDK.htm)
