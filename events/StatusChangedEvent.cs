﻿using System;

namespace TCeye.events
{

    public enum Status
    {
        CONNECTED,
        MOCKED,
        DISCONNECTED
    }

    public class StatusChangedEventArgs : EventArgs
    {
        public Status Status { get; set; }
    }
}
