﻿namespace TCeye
{
    public enum COMMAND
    {
        ADD_REQ,
        ADD_VAL_DEF,
        DEF_REQ,
        TP_REQ,
        WRITE_TEXT,
        START_RECORD,
        RESET_RECORD,
        START_REPLAY,
        SEND_DATABASE,
        INIT_RECORD,
        GET_USER_DATAS
    }

    public enum DEFINITIONS
    {
        Def_POS,
        Def_INIT_POS,
        DEF_WP
    }

    public enum REQUESTS
    {
        REQ_POS,
        REQ_CREATE_AI
    }

    public enum EVENTS
    {
        TXT_EVENT
    }

    public struct POS_struct
    {
        public float kohlsman;
        public float altitude;
        public double latitude;
        public double longitude;
        public float roll;
        public float pitch;
        public float heading;
        public float speed;
    };

}
