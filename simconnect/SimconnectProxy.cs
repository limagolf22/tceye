﻿using Microsoft.FlightSimulator.SimConnect;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Timers;
using TCeye.events;
using TCeye.tools;

namespace TCeye.simconnect
{
    public class SimconnectProxy : SimProxy
    {
        private static System.Timers.Timer Simcotimer;
        private static int q = 0;

        public SimConnect simconnect;

        private float _Theading;
        private POS_struct _lastPOS;
        private POS_struct initPOS;
        private static bool success = false;

        private static bool IsRecording = false, InReviewMode = false, IsPlaying = false, IsRecordInit = false;
        private static int i = 0;

        private static List<POS_struct> POS_buffer = new List<POS_struct>();

        public event EventHandler<Status> ConnectionStatusChanged;

        public event EventHandler<POS_struct> PositionChanged;

        SIMCONNECT_DATA_INITPOSITION position_glob;

        public POS_struct getLastPosOfPlayer()
        {
            return _lastPOS;
        }

        public void initSimEvents(IntPtr _handle, int freqWS)
        {
            const int WM_USER_SIMCONNECT = 0x402;
            try
            {
                simconnect = new SimConnect("Managed Data Request", _handle, WM_USER_SIMCONNECT, null, 0);

            }
            catch (COMException exception1)
            {
                System.Diagnostics.Debug.WriteLine(exception1);

            }

            if (simconnect == null)
            {
                Simcotimer = new System.Timers.Timer(freqWS);
                // Hook up the Elapsed event for the timer. 
                Simcotimer.Elapsed += OnSimcoTimedEvent;
                Simcotimer.AutoReset = true;
                Simcotimer.Enabled = true;
                Simcotimer.Start();
                System.Diagnostics.Debug.WriteLine("Connection to Flight Simulator failed, passing to values default values");
                ConnectionStatusChanged?.Invoke(this, Status.MOCKED);
                return;

            }
            System.Diagnostics.Debug.WriteLine("Connected To Microsoft Flight Simulator 2020!");
            ConnectionStatusChanged?.Invoke(this, Status.CONNECTED);

            simconnect.OnRecvOpen += new SimConnect.RecvOpenEventHandler(simconnect_OnRecvOpen);
            simconnect.OnRecvQuit += new SimConnect.RecvQuitEventHandler(simconnect_OnRecvQuit);
            simconnect.OnRecvException += new SimConnect.RecvExceptionEventHandler(simconnect_OnRecvException);
            //simconnect.OnRecvWaypointList += simconnect_OnRecvWaypointList;

            simconnect.OnRecvEventObjectAddremove += Simconnect_OnRecvEventObjectAddremoveNested;
            simconnect.SubscribeToSystemEvent(DEFINITIONS.Def_INIT_POS, "ObjectAdded");

            simconnect.AddToDataDefinition(DEFINITIONS.Def_POS, "Kohlsman setting hg", "inHg", SIMCONNECT_DATATYPE.FLOAT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.Def_POS, "plane Altitude", "feet", SIMCONNECT_DATATYPE.FLOAT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.Def_POS, "Plane Latitude", "radians", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.Def_POS, "Plane Longitude", "radians", SIMCONNECT_DATATYPE.FLOAT64, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.Def_POS, "Plane Bank Degrees", "radians", SIMCONNECT_DATATYPE.FLOAT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.Def_POS, "Plane Pitch Degrees", "radians", SIMCONNECT_DATATYPE.FLOAT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.Def_POS, "Plane Heading Degrees magnetic", "radians", SIMCONNECT_DATATYPE.FLOAT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);
            simconnect.AddToDataDefinition(DEFINITIONS.Def_POS, "Airspeed Indicated", "knots", SIMCONNECT_DATATYPE.FLOAT32, 0.0f, SimConnect.SIMCONNECT_UNUSED);

            simconnect.AddToDataDefinition(DEFINITIONS.Def_INIT_POS, "Initial Position", "NULL", SIMCONNECT_DATATYPE.INITPOSITION, 0.0f, SimConnect.SIMCONNECT_UNUSED);

            simconnect.AddToDataDefinition(DEFINITIONS.DEF_WP, "AI WAYPOINT LIST", "number", SIMCONNECT_DATATYPE.WAYPOINT, 0.0f, SimConnect.SIMCONNECT_UNUSED);

            System.Diagnostics.Debug.WriteLine("data added");

            simconnect.RegisterDataDefineStruct<POS_struct>(DEFINITIONS.Def_POS);


            simconnect.OnRecvSimobjectData += new SimConnect.RecvSimobjectDataEventHandler(simconnect_OnRecvSimobjectData);


            simconnect.RequestDataOnSimObject(REQUESTS.REQ_POS, DEFINITIONS.Def_POS, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_PERIOD.VISUAL_FRAME, SIMCONNECT_DATA_REQUEST_FLAG.DEFAULT, 0, 0, 0);//---> SIM_FRAME~75Hz


            System.Diagnostics.Debug.WriteLine("request done");

        }

        private void simconnect_OnRecvWaypointList(SimConnect sender, SIMCONNECT_RECV_WAYPOINT_LIST data)
        {
            throw new NotImplementedException();
        }

        private void OnSimcoTimedEvent(object sender, ElapsedEventArgs e)
        {
            q = (q + 1) % 41;
        }

        private void simconnect_OnRecvQuit(SimConnect sender, SIMCONNECT_RECV data)
        {
            System.Diagnostics.Debug.WriteLine("quitting simco");
            ConnectionStatusChanged?.Invoke(this, Status.DISCONNECTED);
            //       pictureBoxSimCo.Image = global::TCeye.Properties.Resources.redDot;
        }

        private void simconnect_OnRecvOpen(SimConnect sender, SIMCONNECT_RECV_OPEN data)
        {
            System.Diagnostics.Debug.WriteLine("opening simco");
            //     pictureBoxSimCo.Image = global::TCeye.Properties.Resources.greenDot;

        }

        private void simconnect_OnRecvException(SimConnect sender, SIMCONNECT_RECV_EXCEPTION data)
        {
            // System.Diagnostics.Debug.WriteLine("exception appear");
            // System.Diagnostics.Debug.WriteLine(data);

        }

        private void simconnect_OnRecvSimobjectData(SimConnect sender, SIMCONNECT_RECV_SIMOBJECT_DATA data)
        {
            switch ((REQUESTS)data.dwRequestID)
            {
                case REQUESTS.REQ_POS:
                    POS_struct provi5 = (POS_struct)data.dwData[0];
                    //POS_STR = "3;" + String.Format(CultureInfo.InvariantCulture, "{0:F6}", provi5.latitude) + ";" + String.Format(CultureInfo.InvariantCulture, "{0:F6}", provi5.longitude) + ";" + String.Format(CultureInfo.InvariantCulture, "{0:F3}", provi5.heading);
                    //System.Diagnostics.Debug.WriteLine("3;" + String.Format(CultureInfo.InvariantCulture, "{0:F6}", provi5.latitude) + ";" + String.Format(CultureInfo.InvariantCulture, "{0:F6}", provi5.longitude) + ";" + String.Format(CultureInfo.InvariantCulture, "{0:F3}", provi5.heading));
                    _Theading = provi5.heading;
                    _lastPOS = provi5;
                    PositionChanged?.Invoke(this, _lastPOS);
                    if (IsRecordInit)
                    {
                        if (POS_buffer.Count >= 20) { POS_buffer.RemoveAt(0); }
                        POS_buffer.Add(provi5);
                    }
                    break;
                default:
                    System.Diagnostics.Debug.WriteLine("message non reconnu");
                    break;
            }
        }

        public void callAIAircraftCreation(POS_struct aircraft_pos)
        {

            SIMCONNECT_DATA_INITPOSITION position = createInitPosFromPosStruct(aircraft_pos);
            //position.Airspeed = SimConnect.INITPOSITION_AIRSPEED_KEEP;
            position_glob = position;

            simconnect.AICreateNonATCAircraft("Asobo LS8 18", "11", position, REQUESTS.REQ_CREATE_AI);
            //simconnect.SubscribeToSystemEvent(DEFINITIONS.Def_INIT_POS, "ObjectAdded");

            //System.Diagnostics.Debug.WriteLine("Trafic created");
            //System.Diagnostics.Debug.WriteLine(position.Latitude);
            //System.Diagnostics.Debug.WriteLine(position.Longitude);
            //System.Diagnostics.Debug.WriteLine(position.Airspeed);
            //System.Diagnostics.Debug.WriteLine(position.Heading);
            //System.Diagnostics.Debug.WriteLine(position.Altitude);
        }

        void Simconnect_OnRecvEventObjectAddremoveNested(SimConnect s, SIMCONNECT_RECV_EVENT_OBJECT_ADDREMOVE p)
        {
            //System.Diagnostics.Debug.WriteLine("object added or removed : " + p.dwData);
            if (p.eObjType == SIMCONNECT_SIMOBJECT_TYPE.AIRCRAFT)
            {
                //changePositionOfObject(p.dwData, position_glob);
                addWaypointsToObject(p.dwData, position_glob);
            }
        }

        /**private Action<SimConnect, SIMCONNECT_RECV_EVENT_OBJECT_ADDREMOVE> Simconnect_OnRecvEventObjectAddremove(SIMCONNECT_DATA_INITPOSITION pos)
        {
            return (SimConnect s, SIMCONNECT_RECV_EVENT_OBJECT_ADDREMOVE p) => changePositionOfObject(p.dwData, pos);
        }*/

        private SIMCONNECT_DATA_INITPOSITION createInitPosFromPosStruct(POS_struct posStruct)
        {
            SIMCONNECT_DATA_INITPOSITION initPos = new SIMCONNECT_DATA_INITPOSITION();
            initPos.Airspeed = (uint)(posStruct.speed);
            initPos.Altitude = posStruct.altitude;
            initPos.Bank = 0.0;
            initPos.Heading = posStruct.heading / Consts.DEG_2_RAD;
            initPos.Latitude = posStruct.latitude / Consts.DEG_2_RAD;
            initPos.Longitude = posStruct.longitude / Consts.DEG_2_RAD;
            initPos.OnGround = 0;
            initPos.Pitch = 0.0;
            return initPos;
        }

        private void changePositionOfObject(uint objectId, SIMCONNECT_DATA_INITPOSITION pos_struct)
        {
            SIMCONNECT_DATA_INITPOSITION pos = pos_struct;
            //pos.altitude += 350;
            //pos.heading -= 1.0f;
            simconnect.SetDataOnSimObject(DEFINITIONS.Def_INIT_POS, objectId, SIMCONNECT_DATA_SET_FLAG.DEFAULT, pos);
            System.Diagnostics.Debug.WriteLine("set data updated with speed : " + pos.Airspeed + "and heading : " + pos.Heading);
        }

        private void addWaypointsToObject(uint objectID, SIMCONNECT_DATA_INITPOSITION pos_struct)
        {
            SIMCONNECT_DATA_WAYPOINT[] waypoints = new SIMCONNECT_DATA_WAYPOINT[2];

            waypoints[0].Flags = (uint)SIMCONNECT_WAYPOINT_FLAGS.ON_GROUND;
            waypoints[0].ktsSpeed = pos_struct.Airspeed;
            waypoints[0].Latitude = pos_struct.Latitude + 0.01 * Math.Cos(pos_struct.Heading * Consts.DEG_2_RAD);
            waypoints[0].Longitude = pos_struct.Longitude + 0.01 * Math.Sin(pos_struct.Heading * Consts.DEG_2_RAD) / Math.Cos(pos_struct.Latitude * Consts.DEG_2_RAD);
            waypoints[0].Altitude = pos_struct.Altitude * 0;

            waypoints[1].Flags = (uint)SIMCONNECT_WAYPOINT_FLAGS.ON_GROUND;
            waypoints[1].ktsSpeed = pos_struct.Airspeed;
            waypoints[1].Latitude = pos_struct.Latitude + 1.2 * Math.Cos(pos_struct.Heading * Consts.DEG_2_RAD);
            waypoints[1].Longitude = pos_struct.Longitude + 1.2 * Math.Sin(pos_struct.Heading * Consts.DEG_2_RAD) / Math.Cos(pos_struct.Latitude * Consts.DEG_2_RAD);
            waypoints[1].Altitude = pos_struct.Altitude;

            Object[] objv = new Object[waypoints.Length];
            waypoints.CopyTo(objv, 0);

            simconnect.SetDataOnSimObject(DEFINITIONS.DEF_WP, objectID, SIMCONNECT_DATA_SET_FLAG.DEFAULT, objv);
            //System.Diagnostics.Debug.WriteLine("set waypoint updated with speed : " + pos_struct.Airspeed + "and heading : " + pos_struct.Heading);
        }

        private void simconnect_parser(string strings)
        {
            var splitted = strings.Split(';');
            switch ((COMMAND)int.Parse(splitted[0]))
            {
                case COMMAND.TP_REQ:
                    POS_struct pos_struct = new POS_struct();
                    pos_struct.kohlsman = 29.92f;
                    pos_struct.altitude = (float)double.Parse(splitted[3], CultureInfo.InvariantCulture);
                    pos_struct.latitude = double.Parse(splitted[1], CultureInfo.InvariantCulture);
                    pos_struct.longitude = double.Parse(splitted[2], CultureInfo.InvariantCulture);
                    pos_struct.roll = 0f;
                    pos_struct.pitch = 0f;
                    pos_struct.heading = _Theading;
                    pos_struct.speed = (float)double.Parse(splitted[4], CultureInfo.InvariantCulture);
                    System.Diagnostics.Debug.WriteLine("received message parsed : " + pos_struct.latitude + "   " + pos_struct.longitude + "   " + pos_struct.speed);

                    simconnect.SetDataOnSimObject(DEFINITIONS.Def_POS, SimConnect.SIMCONNECT_OBJECT_ID_USER, SIMCONNECT_DATA_SET_FLAG.DEFAULT, pos_struct);
                    break;
                case COMMAND.WRITE_TEXT:
                    System.Diagnostics.Debug.WriteLine("received message parsed : " + splitted[1]);
                    simconnect.Text(SIMCONNECT_TEXT_TYPE.PRINT_BLACK, 5.0f, EVENTS.TXT_EVENT, splitted[1]);
                    break;
                default:
                    System.Diagnostics.Debug.WriteLine("received message not recognised");
                    break;
            }
        }
    }

}
