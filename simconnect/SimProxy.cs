﻿using System;

namespace TCeye.simconnect
{
    public interface SimProxy
    {
        POS_struct getLastPosOfPlayer();

        void initSimEvents(IntPtr _handle, int freqWS);

        void callAIAircraftCreation(POS_struct aircraft_pos);
    }
}
